<?php
/**
 * @file
 * Views Facetapi cache
 */

/**
 * Implements hook_views_plugins().
 */
function views_cache_warmer_views_plugins() {
  return array(
    'display_extender' => array(
	'views_cache_warmer_display_extension' => array(
        'path' => drupal_get_path('module', 'views_cache_warmer') . '/views',
        'title' => t('Cache Warming'),
        'help' => t('Warm the views cache on certain events'),
	'handler' => 'views_cache_warmer_plugin_display_extension',
        'uses options' => TRUE,
      ),
    ),
  );
}
