<?php

/**
 * @file
 * The cache warmer display plugin.
 */

class views_cache_warmer_plugin_display_extension extends views_plugin_display_extender {

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_definition_alter(&$options) {
    $options['defaults']['default']['cache_warming'] = FALSE;
    $options['cache_warming']['default'] = FALSE;
  }


  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'cache_warming') {
      $form['cache_warming'] = array(
        '#type' => 'radios',
        '#options' => array(1 => t('Enabled'), 0 => t('Disabled')),
        '#default_value' => $this->display->get_option('cache_warming'),
      );
    }
  }


  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) {
    $this->display->set_option('cache_warming', $form_state['values']['cache_warming']);
  }

  /**
   * Static member function to list which sections are defaultable
   * and what items each section contains.
   */
  function defaultable_sections(&$sections, $section = NULL) {
    $sections['cache_warming'] = array('cache_warming');
  }

  /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    $options['cache_warming'] = array(
      'category' => 'other',
      'title' => t('Cache warming'),
      'value' => $this->display->get_option('cache_warming') ? t('Enabled') : t('Disabled'),
      'desc' => t('Select events to warm the cache on.'),
    );
  }
}
